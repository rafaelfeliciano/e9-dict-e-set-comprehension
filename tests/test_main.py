from main import *


def test_set_comprehension_exercice_1():
    names = ['zÉ', 'aNA', 'jÓ', 'LIA', 'LUan',
             'DAvis', 'GUI', 'fELIpe', 'ANA', 'lia']
    result = set_comprehension_exercice_1(names)
    expected = {'Ana', 'Davis', 'Felipe', 'Gui', 'Lia', 'Luan'}
    assert result == expected


def test_dict_comprehension_exercise_1():
    result = dict_comprehension_exercise_1()
    expected = {3: 3, 6: 6, 9: 9}
    assert result == expected


def test_dict_comprehension_exercise_2():
    ninja_list = ['Kakashi', 'Gai', 'Lee', 'Kiba', 'Tobirama', 'Madara']
    result = dict_comprehension_exercise_2(ninja_list)
    expected = {'Kakashi': 7, 'Tobirama': 8, 'Madara': 6}
    assert result == expected


def test_dict_comprehension_exercise_3():
    my_dict = {'a': 1, 'b': 2, 'c': 3, 'd': 4}
    result = dict_comprehension_exercise_3(my_dict)
    expected = {1: 'a', 2: 'b', 3: 'c', 4: 'd'}
    assert result == expected
