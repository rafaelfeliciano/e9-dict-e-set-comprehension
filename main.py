def set_comprehension_exercice_1(names):
    return set(sorted({name.capitalize() for name in names if len(name) >= 3}))


def dict_comprehension_exercise_1():
    return {n: n for n in range(11) if n % 3 == 0 and n > 0}


def dict_comprehension_exercise_2(ninja_list):
    return {v: len(v) for v in ninja_list if len(v) > 4}


def dict_comprehension_exercise_3(my_dict):
    return {v: k for (k, v) in my_dict.items()}
